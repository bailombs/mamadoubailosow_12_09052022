# SportSee

Sportsee is an application to see you sport data analytics in chart, calculate calories in your meals and more. Sportsee also includes device motion sensor to monitor all of your exercise data and food nutrition in real time.

## Prerequisites

- [Node.js (v16.13.1)](https://nodejs.org/en/)
- [Recommended IDE (Visual Studio Code)](https://code.visualstudio.com)

## Installation

First of all, clone the project with HTTPS

```bash
  git clone https://gitlab.com/bailombs/mamadoubailosow_12_09052022.git
```

The second time you need to install dependencies for the Back_end and the Front_end

### Back-end

Need to be inside the root of the back_end_sport_see folder, and run these commands (install dependencies, and run locally).

> Always start by run the back_end_sport_see before the front_end_sport_see forlder

```
cd back_end_sport_see
yarn install or npm install
yarn dev
```

### Front-end

If you are on the back_end folder, make sure to come back to the root folder of the project with cd ..
So

```
cd front_end_sport_see
yarn install or npm install
yarn start
```

## Generate documentation

In the front_end_sport_see folder you can generate the documentation

```
cd front-end
yarn run docs
```

## Dependencies

| Name          | Version |
| ------------- | ------- |
| react         | 18.1.0  |
| react-dom     | 18.1.0  |
| react-scripts | 5.0.1   |
| react-icons   | 4.3.1   |
| recharts      | 2.1.8   |
| jsdoc         | 3.6.10  |
| axios         | 0.27.2  |
