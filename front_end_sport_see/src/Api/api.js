import axios from "axios"

export const endPoints = {
    infos: 'http://localhost:3000/user/12/',
    performance: 'http://localhost:3000/user/12/performance',
    sessions: 'http://localhost:3000/user/12/average-sessions',
    activity: 'http://localhost:3000/user/12/activity'

}
/**
 *  function to call Api
 * @param {string} endPoint the elements of the endspoins object
 * @returns promise
 */
export const apiCall = (endPoint) => {
    return axios.get(endPoint)
}