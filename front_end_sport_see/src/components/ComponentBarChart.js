import React, { useEffect, useState } from 'react';
import { GoPrimitiveDot } from 'react-icons/go';
import { Bar, BarChart, CartesianGrid, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';
import { apiCall, endPoints } from '../Api/api';
import colors from '../style/_utils.scss';

/**
 *@description display the bar chart Activity
 *@example return(<ComponentBarChart />)
 */
const ComponentBarChart = () => {
    const [userActivity, setUserActivity] = useState([])

    useEffect(() => {

        apiCall(endPoints.activity).then((res) => setUserActivity(res.data.data.sessions))
            .catch(error => console.log(error))
    }, [])

    /**
     *  function Date Formater
     * @param {Date} date  format Date 2022-01-01
     * @returns  {day} - the last digit of the date
     */
    const dateFormater = (date) => {

        let day = date.split('-').pop().slice(1)
        return day
    }

    const CustomToolTip = ({ active, payload }) => {
        if (active && payload && payload.length) {
            return (
                <ul className='custom-barchart-tooltip'>
                    <li className='custom-barchart-tooltip__calories'>{`${payload[0].value} kCal`}</li>
                    <li className='custom-barchart-tooltip__poids'>{`${payload[1].value} Kg`}</li>
                </ul>
            )
        }
    }

    return (
        <div className='container-barchart' style={{ width: '100%', height: 300 }}>
            <ul className='barchartlegend'>
                <li className='barchartlegend__item1'>Activité quotidienne</li>
                <li className='barchartlegend__item2'>
                    <GoPrimitiveDot className='barchartlegend__item2--dot' /> Poids(KG)
                </li>
                <li className='barchartlegend__item3'>
                    <GoPrimitiveDot className='barchartlegend__item3--dot' /> Calories Brûlées (KCal)
                </li>
            </ul>
            <ResponsiveContainer>
                <BarChart

                    height={300}
                    data={userActivity.length && userActivity}
                    barGap={6}
                    margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }}
                >
                    <XAxis dataKey='day' tickFormatter={userActivity.length && dateFormater} />
                    <YAxis
                        axisLine={false}
                        tickLine={false}
                        orientation='right'
                    />


                    <Tooltip content={<CustomToolTip />} />
                    <CartesianGrid stroke="#CCC" vertical={false} opacity={0.4} />
                    <Bar dataKey='kilogram' fill={colors.black4_clr} barSize={8} radius={5} />
                    <Bar dataKey='calories' fill={colors.red1_clr} barSize={8} radius={5} />

                </BarChart>
            </ResponsiveContainer>
        </div>
    );
};

export default ComponentBarChart;











