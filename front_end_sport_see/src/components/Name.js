import React, { useEffect, useState } from 'react';
import { apiCall, endPoints } from '../Api/api';

const Name = () => {
    const [userData, setUserData] = useState("")

    useEffect(() => {
        apiCall(endPoints.infos).then((res) => setUserData(res.data.data))
    }, [])

    return (
        <aside className='name-ctner'>
            <h1 className='name-ctner__name'> Bonjour <span className='name-ctner__name--name'>{userData && userData.userInfos.firstName}</span></h1>
            <p className='name-ctner__text'> Félicitation ! Vous avez explosé vos objectif hier 👏</p>
        </aside>
    );
};

export default Name;

