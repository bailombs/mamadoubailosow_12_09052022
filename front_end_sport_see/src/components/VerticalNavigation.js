import React from 'react';
import { BiCycling, BiSwim } from 'react-icons/bi';
import { CgGym } from 'react-icons/cg';
import { GiMeditation } from 'react-icons/gi';

const VerticalNavigation = () => {
    return (
        <ul className='vertical-nav'>
            <li className='vertical-nav__item'>
                <GiMeditation className='vertical-nav__item--icon' />
            </li>
            <li className='vertical-nav__item'>
                <BiSwim className='vertical-nav__item--icon' />
            </li>
            <li className='vertical-nav__item'>
                <BiCycling className='vertical-nav__item--icon' />
            </li>
            <li className='vertical-nav__item'>
                <CgGym className='vertical-nav__item--icon' />
            </li>
            <li className='vertical-nav__item'>Copyright, SportSee 2022</li>
        </ul>
    );
};

export default VerticalNavigation;