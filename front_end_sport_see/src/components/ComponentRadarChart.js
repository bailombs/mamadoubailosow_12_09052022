import React, { useEffect, useState } from 'react';
import { PolarAngleAxis, PolarGrid, PolarRadiusAxis, Radar, RadarChart, ResponsiveContainer } from 'recharts';
import { apiCall, endPoints } from '../Api/api';


/**
 *@description display the type of Performance
 *@example return(<ComponentRadarChart />)
 */
const ComponentRadarChart = () => {

    const [dataPerformance, setDataPerformance] = useState('')

    /**
     *  Array of object from The API
     * @type {Array<{}>} 
     */
    const data = dataPerformance != null && dataPerformance.data

    /** An object of type Perfomance from the API
     * @type {Object} Kinds
     */
    const kinds = dataPerformance != null && dataPerformance.kind

    useEffect(() => {
        apiCall(endPoints.performance)
            .then((res) => setDataPerformance(res.data.data))
            .catch(error => console.log(error))
    }, [])

    /**
     *  function get the performance type and make the first letter in Uppercase
     * @param {number} kindIndex  the index of the object
     * @returns {string}
     */
    const getKind = (kindIndex) => {
        return kinds && kinds[kindIndex].charAt(0).toUpperCase() + kinds[kindIndex].slice(1)
    }


    return (
        <aside className='radar-chart' style={{ width: '100%', height: 300 }}>
            <ResponsiveContainer >
                <RadarChart
                    outerRadius='50%'
                    width={290}
                    height={300}
                    data={data}
                >
                    <PolarGrid radialLines={false} />
                    <PolarAngleAxis
                        dataKey="kind"
                        tickFormatter={getKind}
                        stroke='#FFFFFF'
                        tickLine={false}
                    />
                    <PolarRadiusAxis angle={30}
                        domain={[0, 150]}
                        stroke='#FFFFFF'
                        tickLine={false}
                        tick={false}
                        axisLine={false}
                    />
                    <Radar dataKey="value" fill="#E60000" fillOpacity={0.7} />
                </RadarChart>
            </ResponsiveContainer>
        </aside>
    );
};

export default ComponentRadarChart;