import React, { useEffect, useState } from 'react';
import { Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';
import { apiCall, endPoints } from '../Api/api';

/**
 *@description display the Average-Session(duration)
 *@example return(<ComponentLineChart />)
 */
const ComponentLineChart = () => {
  const [sessions, setSessions] = useState([])

  useEffect(() => {
    apiCall(endPoints.sessions)
      .then((res) => setSessions(res.data.data.sessions))
      .catch(error => console.log(error))
  }, [])

  const days = {
    1: 'L',
    2: 'M',
    3: 'M',
    4: 'J',
    5: 'V',
    6: 'S',
    7: 'D'
  }

  /**
   * 
   * @param {number} key the index of the array
   * @returns 
   */
  const getDay = (key) => {
    return days[key]
  }

  const CustomLineToolTip = ({ active, payload }) => {
    return (
      active && payload && payload.length ?
        <p className='line-chart__tooltip'>{`${payload[0].value} min`}</p> : null
    )
  }

  return (
    <aside className='line-chart' style={{ width: '100%', height: 300 }}>
      <p className='line-chart__title'>Durée Moyenne des sessions</p>
      <ResponsiveContainer >
        <LineChart
          width={290}
          height={250}
          data={sessions.length && sessions}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <XAxis dataKey="day"
            tickFormatter={getDay}
            axisLine={false}
            tickLine={false}
            stroke='#FFFFFF'
            height={100}
          />
          <YAxis axisLine={false}
            tickLine={false}
            hide='false'
            domain={['dataMin']}
          />
          <Tooltip content={<CustomLineToolTip />} />
          <Line type="monotone"
            dataKey="sessionLength"
            strokeWidth={2}
            stroke='#FFFFFF'
            activeDot={{ fill: "#FFFFFF", stroke: 'rgba(255,255,255,0.4)', strokeWidth: 10, r: 7 }}
            dot={false}
          />
        </LineChart>
      </ResponsiveContainer>

    </aside>
  );
};

export default ComponentLineChart;