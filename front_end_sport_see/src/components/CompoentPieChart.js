import React, { useEffect, useState } from 'react';
import { Cell, Pie, PieChart, ResponsiveContainer } from 'recharts';
import { apiCall, endPoints } from '../Api/api';
import colors from '../style/_utils.scss';

/**
 *@description display the score
 *@example return(<ComponentPieChart />)
 */
const CompoentPieChart = () => {
    const [todayScore, setTodayScore] = useState(0)

    useEffect(() => {
        apiCall(endPoints.infos)
            .then((res) => setTodayScore(res.data.data.todayScore))
            .catch(error => console.log(error))
    }, [])


    /**
     * Array of object to follow the circular progression
     * @type {Array<{}>}
     */
    const data = [
        { title: "circumference", value: 1 - todayScore, fillColor: colors.white_clr },
        { title: 'progress', value: todayScore, fillColor: colors.red2_clr },
    ];

    return (
        <aside className='pie-chart' style={{ width: '100%', height: 300 }}>
            <p className='pie-chart__score'> Score</p>
            <ul className='pie-chart__legend'>
                <li className='pie-chart__legend--percent'>{`${todayScore * 100}%`}</li>
                <li className='pie-chart__legend--text'>de votre<br />Objectif</li>
            </ul>
            <ResponsiveContainer >
                <PieChart width={290} height={250}>
                    <Pie
                        data={data}
                        cx='50%'
                        cy='50%'
                        startAngle={180}
                        endAngle={180 + 360}
                        innerRadius={60}
                        outerRadius={68}
                        fill="white"
                        paddingAngle={false}
                        dataKey="value"
                    >
                        {data.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={entry.fillColor} />
                        ))}
                    </Pie>
                </PieChart>
            </ResponsiveContainer>

        </aside>
    );
};

export default CompoentPieChart;