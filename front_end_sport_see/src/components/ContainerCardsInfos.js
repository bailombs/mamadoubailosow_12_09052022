import React, { useEffect, useState } from 'react';
import { FaAppleAlt, FaHamburger } from 'react-icons/fa';
import { GiChickenLeg } from 'react-icons/gi';
import { ImFire } from 'react-icons/im';
import { apiCall, endPoints } from '../Api/api';
import colors from '../style/_utils.scss';
import CardInfos from './CardInfo';
/**
 *@description component display the list of cards infos
 *@example return(<ContainerCardsInfos />)
 */
const ContainerCardsInfos = () => {
    const [keyData, setKeyData] = useState('')

    useEffect(() => {
        apiCall(endPoints.infos)
            .then((res) => setKeyData(res.data.data.keyData))
            .catch(error => console.log(error))
    }, [])


    /**
     * Array of Object for the cardsInfos
     * @type {Array<{icon: object, count: number, unit: string, name: string, iconBgdColor: string}>}
     */
    const cardsInfos = [
        {
            icon: <ImFire style={{ color: `${colors.red2_clr}` }} />,
            count: keyData.calorieCount,
            unit: 'kCal',
            name: 'Calories',
            iconBgColor: 'calorie-bgcolor'
        },

        {
            icon: <GiChickenLeg style={{ color: `${colors.blue_clr}` }} />,
            count: keyData.proteinCount,
            unit: 'g',
            name: 'Proteïne',
            iconBgColor: 'protein-bgcolor'
        },

        {
            icon: <FaAppleAlt style={{ color: `${colors.yellow_clr}` }} />,
            count: keyData.carbohydrateCount,
            unit: 'g',
            name: 'Glucides',
            iconBgColor: 'carbohydrate-bgcolor'
        },

        {
            icon: <FaHamburger style={{ color: `${colors.red3_clr}` }} />,
            count: keyData.lipidCount,
            unit: 'g',
            name: 'Lipides',
            iconBgColor: 'lipid-bgcolor'
        }
    ]


    return (
        <aside className='ctn-cards-infos'>
            {keyData && cardsInfos.map((cardInfo) =>
                <CardInfos key={cardInfo.name}
                    icon={cardInfo.icon}
                    number={cardInfo.count}
                    unit={cardInfo.unit}
                    name={cardInfo.name}
                    iconBgdColor={cardInfo.iconBgColor}
                />
            )}
        </aside>
    );
};

export default ContainerCardsInfos;