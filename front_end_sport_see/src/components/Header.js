import React from 'react';
import HorizontalNavigation from './HorizontalNavigation';

const Header = () => {
    return (
        <header className='header'>
            <HorizontalNavigation />
        </header>
    );
};

export default Header;