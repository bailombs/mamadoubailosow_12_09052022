import React from 'react';
import logo from '../assets/images/logo.png';
const HorizontalNavigation = () => {
    return (
        <ul className='header__nav'>
            <li className='header__nav--logo'>
                <img src={logo} alt="logo" />
            </li>
            <li>Accueil</li>
            <li>Profil</li>
            <li>Réglage</li>
            <li>Communauté</li>
        </ul>
    );
};

export default HorizontalNavigation;