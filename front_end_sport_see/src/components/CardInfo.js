import PropTypes from 'prop-types';
import React from 'react';

/**
 *@description dispplay the component of card info
 *@example return(<CardInfo />)
 */
const CardInfo = ({ icon, number, unit, name, iconBgdColor }) => {

    return (
        <ul className='card-infos'>
            <li className={`card-infos__icon ${iconBgdColor}`} >
                {icon}
            </li>
            <li className='card-infos__count'>{number}<span>{unit}</span></li>
            <li className='card-infos__name'> {name}</li>
        </ul>
    );
};

CardInfo.propTypes = {
    icon: PropTypes.object.isRequired,
    number: PropTypes.number.isRequired,
    unit: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    iconBgdColor: PropTypes.string.isRequired
}

export default CardInfo;