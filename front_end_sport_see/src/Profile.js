import React from 'react';
import CompoentPieChart from './components/CompoentPieChart';
import ComponentBarChart from './components/ComponentBarChart';
import ComponentLineChart from './components/ComponentLineChart.js';
import ComponentRadarChart from './components/ComponentRadarChart';
import ContainerCardsInfos from './components/ContainerCardsInfos';
import Header from './components/Header';
import Name from './components/Name';
import VerticalNavigation from './components/VerticalNavigation';
import './style/index.scss';


const Profile = () => {
  return (
    <React.Fragment>
      <Header />
      <main className='main'>
        <VerticalNavigation />
        <section className='middle-section' >
          <Name />
          <ComponentBarChart />
          <div className='cards-chart'>
            <ComponentLineChart />
            <ComponentRadarChart />
            <CompoentPieChart />
          </div>
        </section>
        <ContainerCardsInfos />
      </main>
    </React.Fragment>
  );
};

export default Profile;